//make express application
//assign port to that application
import express, { json } from "express";
import firstRouter from "./src/Router/firstRouter.js";
import taxi from "./src/Router/secondRouter.js";
import bike from "./src/Router/bike.js";
import trainee from "./src/Router/trainee.js";
import vehicle from "./src/Router/vehicle.js";




let expressApp = express(); //app is created

expressApp.use(json());     //always place this code at top of the router eg. expressApp.use("/", firstRouter);

let port = 8000;
expressApp.listen(port, () => {
  //port is assigned to our app
  console.log(`Express App is listening at port ${port}`)
  
});



// Application Middleware
// expressApp.use(
//   (req,res,next)=>{
//     console.log("i am application middleware 1")
//     next()
//   },
//   (req,res,next)=>{
//     console.log("i am application middleware 2")
//     next()
//   },
//   (req,res,next)=>{
//     console.log("i am application middleware 3")
//     next()
//   }
//   )



expressApp.use("/", firstRouter);
expressApp.use("/bike", bike);
expressApp.use("/trainee", trainee);
expressApp.use("/vehicle", vehicle);
expressApp.use("/taxi",taxi)

//send data from postman
// -->body = req.body
// -->query = req.query
//  -->params = req.params

//url
//localhost:8000/students?name=manish&age=25
//url = route+query
//route = localhost:8000/students (student route parameter)
// route parameter is divided into two parts
//one is static route parameter > must use same name as mention
//another one is dynamic route parameter > can use any name
//query = manish&age=25  =>query parameter
//whatever we pass in query, it always takes values as string


//always place API (which has dynamic route parameter) at last