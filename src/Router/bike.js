import { Router } from "express";

let bike = Router()

bike
.route("/")
.post(
    (req,res,next)=>{
        console.log("i am middleware 1")
        next("a")
    },
    (err,req,res,next)=>{
        console.log("i am middleware 2")
        next([1,2])
    },
    (err,req,res,next)=>{
        console.log("i am middleware 3")
        res.json("h1")
    },

)






.get(()=>{
    console.log("bike get")
})
.patch(()=>{
    console.log("bike patch")
})
.delete(()=>{
    console.log("bike delete")
})

export default bike



// Middleware are the functions which has req, res and next
//to trigger next middleware we have to call next()

//middleware types
// Normal middleware => req,res,next, to trigger we call next()
// error middleware => err,req,res,next, to trigger we call next("err") ie. next() with parameter

